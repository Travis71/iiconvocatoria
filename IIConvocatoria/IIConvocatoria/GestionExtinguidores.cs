﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IIConvocatoria
{
    public partial class GestionExtinguidores : Form
    {
        DataSet dsExt;
        BindingSource bsExt = new BindingSource();
        Ext ext;

        public DataSet DsExt
        {
            get
            {
                return dsExt;
            }

            set
            {
                dsExt = value;
            }
        }

        internal Ext Ext
        {
            get
            {
                return ext;
            }

            set
            {
                ext = value;
            }
        }

        public GestionExtinguidores()
        {
            InitializeComponent();
        }

        private void GestionExtinguidores_Load(object sender, EventArgs e)
        {
            bsExt.DataSource = DsExt;
            bsExt.DataMember = DsExt.Tables["Extinguidores"].TableName;
            dgvExt.DataSource = bsExt;
            dgvExt.AutoGenerateColumns = true;
        }
    }
}
