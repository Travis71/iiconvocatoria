﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IIConvocatoria
{
    public partial class MainForm : Form
    {
        GestionExtinguidores ge = new GestionExtinguidores();
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            
            ge.DsExt = dsPrincipal;
            ge.MdiParent = this;
            ge.Show();
        }

        private void agregarStripMenuItem_Click(object sender, EventArgs e)
        {
            AgregarExt ae = new AgregarExt(dsPrincipal.Tables["Extinguidores"].Rows.Count);
            ae.ShowDialog();
            if(ae.Ext != null)
            {
                DataRow dr = dsPrincipal.Tables["Extinguidores"].NewRow();
                dr[0] = ae.Ext.Id;
                dr[1] = ae.Ext.Categoria;
                dr[2] = ae.Ext.Marca;
                dr[3] = ae.Ext.Capacidad;
                dr[4] = ae.Ext.Tipo;
                dr[5] = ae.Ext.Unidad_medida;
                dr[6] = ae.Ext.Cantidad;
                dsPrincipal.Tables["Extinguidores"].Rows.Add(dr);
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GestionExtinguidores get = new GestionExtinguidores();
            get.DsExt = dsPrincipal;
            get.MdiParent = this;
            get.Show();
        }

        private void reporteDeCO2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reporte r = new Reporte(0);
            r.MdiParent = this;

        }
    }
}
