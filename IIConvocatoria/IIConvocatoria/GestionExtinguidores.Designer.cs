﻿namespace IIConvocatoria
{
    partial class GestionExtinguidores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvExt = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExt)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvExt
            // 
            this.dgvExt.AllowUserToAddRows = false;
            this.dgvExt.AllowUserToDeleteRows = false;
            this.dgvExt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvExt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvExt.Location = new System.Drawing.Point(0, 0);
            this.dgvExt.MultiSelect = false;
            this.dgvExt.Name = "dgvExt";
            this.dgvExt.ReadOnly = true;
            this.dgvExt.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvExt.Size = new System.Drawing.Size(748, 329);
            this.dgvExt.TabIndex = 0;
            // 
            // GestionExtinguidores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(748, 329);
            this.Controls.Add(this.dgvExt);
            this.MinimizeBox = false;
            this.Name = "GestionExtinguidores";
            this.ShowIcon = false;
            this.Text = "GestionExtinguidores";
            this.Load += new System.EventHandler(this.GestionExtinguidores_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExt)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvExt;
    }
}