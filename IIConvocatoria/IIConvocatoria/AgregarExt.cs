﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IIConvocatoria
{
    public partial class AgregarExt : Form
    {
        private Ext ext = new Ext();
        int index;
        public AgregarExt(int i)
        {
            InitializeComponent();
            this.index = i;
        }

        internal Ext Ext
        {
            get
            {
                return ext;
            }

            set
            {
                ext = value;
            }
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox4.SelectedIndex == 0) textBox1.Text = "Libras";
            else textBox1.Text = "Litros";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ext = null;
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((string)comboBox1.SelectedItem != null
                && (string)comboBox2.SelectedItem != null
                && (string)comboBox3.SelectedItem != null
                && (string)comboBox4.SelectedItem != null
                && numericUpDown1.Value != 0)
            {
                Ext.Id = index + 1;
                Ext.Categoria = (string)comboBox1.SelectedItem;
                Ext.Marca = (string)comboBox2.SelectedItem;
                Ext.Capacidad = (string)comboBox3.SelectedItem;
                Ext.Tipo = (string)comboBox4.SelectedItem;
                Ext.Unidad_medida = textBox1.Text;
                Ext.Cantidad = (int)numericUpDown1.Value;
                Close();
            }
            else label7.Text = "Campos Incompletos";
        }

        private void AgregarExt_Load(object sender, EventArgs e)
        {

        }
    }
}
