﻿namespace IIConvocatoria
{
    partial class MainForm
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.inicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.agregarStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteDeCO2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteDeH2OToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteDeQSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hToolStripMenuItem = new System.Windows.Forms.ToolStripSeparator();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dsPrincipal = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inicioToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(538, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // inicioToolStripMenuItem
            // 
            this.inicioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.agregarStripMenuItem,
            this.reporteToolStripMenuItem,
            this.hToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.inicioToolStripMenuItem.Name = "inicioToolStripMenuItem";
            this.inicioToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.inicioToolStripMenuItem.Text = "Inicio";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(197, 22);
            this.toolStripMenuItem1.Text = "Visualizar Extinguidores";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // agregarStripMenuItem
            // 
            this.agregarStripMenuItem.Name = "agregarStripMenuItem";
            this.agregarStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.agregarStripMenuItem.Text = "Agregar extinguidor";
            this.agregarStripMenuItem.Click += new System.EventHandler(this.agregarStripMenuItem_Click);
            // 
            // reporteToolStripMenuItem
            // 
            this.reporteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reporteDeCO2ToolStripMenuItem,
            this.reporteDeH2OToolStripMenuItem,
            this.reporteDeQSToolStripMenuItem});
            this.reporteToolStripMenuItem.Name = "reporteToolStripMenuItem";
            this.reporteToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.reporteToolStripMenuItem.Text = "Reporte";
            // 
            // reporteDeCO2ToolStripMenuItem
            // 
            this.reporteDeCO2ToolStripMenuItem.Name = "reporteDeCO2ToolStripMenuItem";
            this.reporteDeCO2ToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.reporteDeCO2ToolStripMenuItem.Text = "Reporte de CO2";
            this.reporteDeCO2ToolStripMenuItem.Click += new System.EventHandler(this.reporteDeCO2ToolStripMenuItem_Click);
            // 
            // reporteDeH2OToolStripMenuItem
            // 
            this.reporteDeH2OToolStripMenuItem.Name = "reporteDeH2OToolStripMenuItem";
            this.reporteDeH2OToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.reporteDeH2OToolStripMenuItem.Text = "Reporte de H2O";
            // 
            // reporteDeQSToolStripMenuItem
            // 
            this.reporteDeQSToolStripMenuItem.Name = "reporteDeQSToolStripMenuItem";
            this.reporteDeQSToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.reporteDeQSToolStripMenuItem.Text = "Reporte de QS";
            // 
            // hToolStripMenuItem
            // 
            this.hToolStripMenuItem.Name = "hToolStripMenuItem";
            this.hToolStripMenuItem.Size = new System.Drawing.Size(194, 6);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            // 
            // dsPrincipal
            // 
            this.dsPrincipal.DataSetName = "NewDataSet";
            this.dsPrincipal.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7});
            this.dataTable1.TableName = "Extinguidores";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "ID";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "Categoria";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "Marca";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "Capacidad";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "Tipo";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "Unidad de medida";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "Cantidad";
            this.dataColumn7.DataType = typeof(int);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 352);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Extinguidores";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem inicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reporteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reporteDeCO2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reporteDeH2OToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reporteDeQSToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator hToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agregarStripMenuItem;
        private System.Data.DataSet dsPrincipal;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    }
}

