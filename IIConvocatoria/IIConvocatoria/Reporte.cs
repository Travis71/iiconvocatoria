﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IIConvocatoria
{
    public partial class Reporte : Form
    {
        int tipo;
        DataSet dsExt;

        public DataSet DsExt
        {
            get
            {
                return dsExt;
            }

            set
            {
                dsExt = value;
            }
        }

        public Reporte(int tipotemp)
        {
            this.tipo = tipotemp;
            InitializeComponent();
        }

        private void Reporte_Load(object sender, EventArgs e)
        {
            if(tipo == 0) this.reportViewer1.LocalReport.ReportEmbeddedResource = "IIConvocatoria.CO2.rdlc";
            else if(tipo == 1) this.reportViewer1.LocalReport.ReportEmbeddedResource = "IIConvocatoria.H2O.rdlc";
            else if(tipo == 2) this.reportViewer1.LocalReport.ReportEmbeddedResource = "IIConvocatoria.QS.rdlc";
            this.reportViewer1.RefreshReport();
        }
    }
}
